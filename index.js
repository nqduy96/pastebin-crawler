/*
 *  Create By: ImRobott
 */

const fs = require('fs');
const Nightmare = require('nightmare')
const nightmare = Nightmare({ show: true })
  
// Run
nightmare
  .goto('https://pastebin.com/u/malware_traffic')
  .wait('table.maintable')
  .evaluate(() => [...document.querySelectorAll(".pagination a")].map(element => element.href))
  .then(async listPage => {
    var myJson = []

    // Remove duplicate in listPage
    listPage = listPage.filter((elem, index, self) => {
      return index === self.indexOf(elem);
    })

    // Crawl pages
    for (const page of listPage) await myJsonCrawlerStepByStep(nightmare, myJson, page)

    // Write myJson file
    fs.writeFileSync('./result/myJson.json', JSON.stringify(myJson, null, 2))

    //  Content Crawler 
    for (const row of myJson) {
      const {name_title, link} = row
      var fileName = name_title
      // remove special characters 
      fileName = fileName.split(" ...").join("...")
      fileName = fileName.split("/").join("")
      fileName = fileName.split('"').join("")

      const textContent = await contentCrawlerStepByStep(nightmare, link)
      fs.writeFileSync(`./result/${fileName}`, textContent)
    }

    console.log("Done!!!")
    return nightmare.end()
  })
  .catch(error => {
    console.error('Search failed:', error)
})

/*
 *  My Function
 */

const myJsonCrawlerStepByStep = (nightmare, myJson, href) => {
  return new Promise((resolve, reject) => {
    nightmare
      .goto(href)
      .evaluate(function() {                  //wait for page to fully load
        return false;
      })
      .wait('table.maintable')
      .evaluate(() => {
        const links = [...document.querySelectorAll('.maintable > tbody tr > td:first-child a')].map(element => element.href)
        const listNameTitle = [...document.querySelectorAll('.maintable > tbody tr > td:first-child a')].map(element => element.innerHTML)
        const listDate = [...document.querySelectorAll('.maintable > tbody tr > td:nth-child(2)')].map(element => element.innerHTML)
        return {links, listNameTitle, listDate}
      })
      .then(data => {
        const { links, listNameTitle, listDate } = data
        // Check length
        const needToCheck = [links.length, listNameTitle.length, listDate.length]
        const allEqual = needToCheck.every(v => v === needToCheck[0])
        if (!allEqual) {
          console.log("Some thing wrong !!!")
          return nightmare.end()
        }

        // update myJson
        links.forEach((link, index) => {
          const rowJson = {
            'name_title': listNameTitle[index],
            'link': link,
            'date': listDate[index]
          }
          myJson.push(rowJson)
        });

        resolve(myJson)
      })
  })
}

const contentCrawlerStepByStep = (nightmare, href) => {
  return new Promise((resolve, reject) => {
    nightmare
      .goto(href)
      .evaluate(function() {                  //wait for page to fully load
        return false;
      })
      .wait('#selectable')
      .evaluate(() => document.querySelector('#selectable .text').textContent)
      .then(textContent => resolve(textContent)) 
  })
}